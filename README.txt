DESCRIPTION:

Due to the large variety of ions force fields available in the literature used in MD simulations,
we have decided to make a collection of them so they can be properly used and referenced.

Here you will find the topology of many ions force fields published in the literature
ready to be used in you projects. As many results seem to be very dependent in the
ions used please ensure to especify in your publications which one you used. Enjoy!!

Subscribe to the repository if you want to receive warnings when new ions are available.


GROMACS
---------
1.- Copy the content of the gromacs folder inside a "top" folder placed in your simulation folder.
2.- Add the following lines in your top file:

   2.a.- Force fields that use sigma and epsilon for the Van der Waals parameters.
   Lines to add:
   #include <./top/ions_nb_sigma_eps.itp> ; This file MUST be included before any molecule definition.
   #include <./top/ions.itp>

   List of force fields using sigma and epsilon: Amber, Charmm , OPLSaa

   2.b.-  Force fields that use C6 and C12 for the Van der Waals parameters
   Lines to add:

   #include <./top/ions_nb_C6_C12.itp> ; This file MUST be included before any molecule definition.
   #include <./top/ions.itp>

   List of force fields using C6 and C12: Gromos


